# Extractor

## Introduction

This repository is responsible to extract data from external data sources.

## Requirements:

- Python 3.7
- Docker
- Docker Compose

## Running the application locally

Build the images by docker-compose:

```sh
$ docker-compose build
```

Up containers:

```
$ docker-compose up -d
```

And to run application:

```sh
$ docker-compose exec app ./run_spark_submit.sh
```

## AWS Credentials

To boto3 works is necessary configure [aws cli](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/install-cliv2.html) in local machine.