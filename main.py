""" Extractor Module """

import logging
import sys
import time
import datetime
import boto3
from awsglue.utils import getResolvedOptions
from pyspark.sql import SparkSession
from pyspark.sql.functions import (date_format,
                                   col, lit,
                                   unix_timestamp)
from log.log import conf_logging

conf_logging("info")
LOGGER = logging.getLogger(__name__)


def get_parameter_store(name: str, with_decryption: bool = False) -> str:
    """Get value name from aws parameter store

    Returns:
        str: value name from aws parameter store
    """
    LOGGER.info(
        " ".join(
            f"Get value of parameter store from name={name} \
                and with_decryption={with_decryption}"
            .strip().split()))
    client = boto3.client('ssm')
    value = client.get_parameter(Name=name, WithDecryption=with_decryption)
    return value["Parameter"]["Value"]


ARGS = getResolvedOptions(sys.argv, ["table_name", "path_to_datalake"])
TABLE = ARGS["table_name"]
DATABASE = "gdu"
HOST = get_parameter_store("host-gdu")
PORT = get_parameter_store("port-gdu")
USER = get_parameter_store("user-gdu")
PASSWORD = get_parameter_store("password-gdu", True)
DST = ARGS['path_to_datalake']
DATE_TIME_0_NULL = "zeroDateTimeBehavior=convertToNull"
BUCKET_NAME = "nc-datablue-qa-trusted"
FOLDER_NAME = "canonical-raw"


def get_timestamp_dict(timestamp: str) -> dict:
    """Convert timestamp to dict

    Args:
        timestamp (str): ingestion timestamp

    Returns:
        dict: dict timestamp with the following keys:
        year, month, day and hour_mm_ss
    """
    timestamp_arr = timestamp.split("-")
    year, month = timestamp_arr[:2]
    day, hour_mm_ss = timestamp_arr[-1].split(" ")
    hour_mm_ss = hour_mm_ss.replace(":", "")
    return {"year": year, "month": month,
            "day": day, "hour_mm_ss": hour_mm_ss}


def ingestion():
    """ Data ingestion """
    timestamp = datetime.datetime.fromtimestamp(
        time.time()).strftime('%Y-%m-%d %H:%M:%S')

    spark = SparkSession.builder.getOrCreate()
    LOGGER.info(TABLE)
    dataframe = spark.read.option("driver",
                                    "com.mysql.cj.jdbc.Driver").jdbc(
        url=f"jdbc:mysql://{HOST}:{PORT}/{DATABASE}?{DATE_TIME_0_NULL}",
        table=TABLE, properties={"user": USER, "password": PASSWORD})
    dataframe = dataframe.withColumn("timestamp",
                                        unix_timestamp(
                                            lit(timestamp),
                                            'yyyy-MM-dd HH:mm:ss')
                                        .cast("timestamp")) \
        .withColumn("year", date_format(col("timestamp"), "yyyy")) \
        .withColumn("month", date_format(col("timestamp"), "MM")) \
        .withColumn("day", date_format(col("timestamp"), "dd")) \
        .withColumn("hour_mm_ss", date_format(
            col("timestamp"), "HHmmss")) \
        .drop("timestamp")
    dataframe.write.mode("overwrite").partitionBy(
        "year", "month", "day", "hour_mm_ss").parquet(f"{DST}/{TABLE}")
    LOGGER.info(f"Table {TABLE} was created!")


ingestion()
